logs:
	docker-compose logs -f postgres
ssh:
	docker-compose exec postgres /bin/bash
psql:
	docker-compose exec postgres psql -U postgres
job:
	docker-compose exec postgres psql -U postgres -f /scripts/job.sql
number-transactions:
	docker-compose exec postgres psql -U postgres -c 'SELECT COUNT(1) FROM financial.transactions;'
run:
	docker-compose exec postgres psql -U postgres -c "SET search_path TO financial; CALL sp_transactions(1, '1');"
start:
	docker-compose exec postgres /scripts/start
dump:
	docker-compose exec postgres pg_dump -U postgres -s --schema=financial --schema=public --format plain --encoding UTF8
rebuild:
	docker-compose stop && docker-compose rm -f && docker-compose up -d
