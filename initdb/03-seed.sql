SET search_path TO financial;

-- Auto-generated SQL script #202010041745
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','01000-1',100000,'1');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','02000-2',100000,'1');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','03000-3',100000,'1');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','04000-4',100000,'1');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','05000-5',100000,'1');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','06000-6',100000,'1');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0001','07000-7',100000,'1');

INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0002','08000-1',100000,'2');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0002','09000-2',100000,'2');
INSERT INTO financial.accounts (bank_branch,account_number,balance,account_type) VALUES ('0002','10000-3',100000,'2');

DO $$
DECLARE p_account_type financial.account_types;
BEGIN
    -- 500000
    SELECT CONCAT(floor(random()*2)+1, '') INTO p_account_type;
    call sp_transactions(500, p_account_type);

    SELECT CONCAT(floor(random()*2)+1, '') INTO p_account_type;
    call sp_transactions(500, p_account_type);

    SELECT CONCAT(floor(random()*2)+1, '') INTO p_account_type;
    call sp_transactions(500, p_account_type);

    SELECT CONCAT(floor(random()*2)+1, '') INTO p_account_type;
    call sp_transactions(500, p_account_type);
END $$;
