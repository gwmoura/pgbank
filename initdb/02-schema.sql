docker-compose exec postgres pg_dump -U postgres -s --schema=financial --schema=public --format plain --encoding UTF8
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Debian 12.4-1.pgdg100+1)
-- Dumped by pg_dump version 12.4 (Debian 12.4-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: financial; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA financial;


ALTER SCHEMA financial OWNER TO postgres;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: account_types; Type: TYPE; Schema: financial; Owner: postgres
--

CREATE TYPE financial.account_types AS ENUM (
    '1',
    '2'
);


ALTER TYPE financial.account_types OWNER TO postgres;

--
-- Name: operations_status_list; Type: TYPE; Schema: financial; Owner: postgres
--

CREATE TYPE financial.operations_status_list AS ENUM (
    'pending',
    'in_progress',
    'finished'
);


ALTER TYPE financial.operations_status_list OWNER TO postgres;

--
-- Name: transaction_types; Type: TYPE; Schema: financial; Owner: postgres
--

CREATE TYPE financial.transaction_types AS ENUM (
    'D',
    'C'
);


ALTER TYPE financial.transaction_types OWNER TO postgres;

--
-- Name: sp_job_process_transactions(); Type: PROCEDURE; Schema: financial; Owner: postgres
--

CREATE PROCEDURE financial.sp_job_process_transactions()
    LANGUAGE plpgsql
    AS $$
  declare v_msg text;
  begin
    loop
      call sp_process_transactions(v_msg);
      commit;

      if v_msg <> '' then
        exit;
      end if;
    end loop;
  end;
$$;


ALTER PROCEDURE financial.sp_job_process_transactions() OWNER TO postgres;

--
-- Name: sp_process_transactions(text); Type: PROCEDURE; Schema: financial; Owner: postgres
--

CREATE PROCEDURE financial.sp_process_transactions(INOUT p_msg text)
    LANGUAGE plpgsql
    AS $$

  declare v_amount bigint;
  declare v_from_account_id int;
  declare v_to_account_id int;
  declare v_id uuid;

  begin
    p_msg = '';
  
    select id, from_account_id, to_account_id, amount
      into v_id, v_from_account_id, v_to_account_id, v_amount
    from financial.transactions_queue
    where operation_status = 'pending'
    order by created_at asc
    limit 1
    for update skip locked;

    if v_id is not null then
      update financial.transactions_queue
        set operation_status = 'in_progress'
      where id = v_id;

      call sp_transfer(v_from_account_id, v_to_account_id, v_amount);

      update financial.transactions_queue
        set operation_status = 'finished',
            updated_at = current_timestamp
      where id = v_id;

      commit;
    else
      p_msg = 'Nenhum item para processar';
      RAISE NOTICE 'Nenhum item para processar';
    end if;
  end;
$$;


ALTER PROCEDURE financial.sp_process_transactions(INOUT p_msg text) OWNER TO postgres;

--
-- Name: sp_register_transfer(integer, integer, bigint); Type: PROCEDURE; Schema: financial; Owner: postgres
--

CREATE PROCEDURE financial.sp_register_transfer(sender_id integer, receiver_id integer, amount bigint)
    LANGUAGE plpgsql
    AS $$

  begin
    insert into financial.transactions_queue (from_account_id, to_account_id, amount)
    values(sender_id, receiver_id, amount);
  end;
$$;


ALTER PROCEDURE financial.sp_register_transfer(sender_id integer, receiver_id integer, amount bigint) OWNER TO postgres;

--
-- Name: sp_transactions(bigint, financial.account_types); Type: PROCEDURE; Schema: financial; Owner: postgres
--

CREATE PROCEDURE financial.sp_transactions(times bigint, p_account_type financial.account_types)
    LANGUAGE plpgsql
    AS $$

  declare current_index int;
  declare sender_id int;
  declare receiver_id int;
  declare total_accounts int;
  declare amount bigint;

  begin
    current_index = 0;

    select count(1)-1 into total_accounts
    from accounts
    where account_type = p_account_type;

    while current_index <= times loop

      RAISE LOG 'executing transaction %', current_index;

      select id into sender_id
      from accounts
      where account_type = p_account_type
      offset floor(random()*total_accounts)
      limit 1;

      select id into receiver_id
      from accounts
      where account_type = p_account_type
      offset floor(random()*total_accounts)
      limit 1;

      amount = (random() * 100000 + 1)::bigint;

      call sp_register_transfer(sender_id, receiver_id, amount);

      current_index = current_index + 1;

    end loop;
  end;
$$;


ALTER PROCEDURE financial.sp_transactions(times bigint, p_account_type financial.account_types) OWNER TO postgres;

--
-- Name: sp_transfer(integer, integer, bigint); Type: PROCEDURE; Schema: financial; Owner: postgres
--

CREATE PROCEDURE financial.sp_transfer(sender_id integer, receiver_id integer, amount bigint)
    LANGUAGE plpgsql
    AS $$

  declare sender_balance bigint;
  declare receiver_balance bigint;

  begin
    select balance into sender_balance from financial.accounts where id = sender_id;
    select balance into receiver_balance from financial.accounts where id = receiver_id;

    sender_balance = sender_balance - amount;
    receiver_balance = receiver_balance + amount;
  
    if sender_id = receiver_id then
      insert into financial.transactions (from_account_id, to_account_id, value, transaction_type)
      values(sender_id, receiver_id, amount, 'C');

      update accounts
      set balance = receiver_balance
      where id = receiver_id;
    elseif sender_balance >= 0 and sender_id != receiver_id then
      -- subtracting the amount from the sender's account
      insert into financial.transactions (from_account_id, to_account_id, value, transaction_type)
      values(sender_id, sender_id, amount, 'D');

      update accounts
      set balance = sender_balance
      where id = sender_id;

      -- adding the amount to the receiver's account
      insert into financial.transactions (from_account_id, to_account_id, value, transaction_type)
      values(sender_id, receiver_id, amount, 'C');

      update accounts
      set balance = receiver_balance
      where id = receiver_id;

    end if;
  end;
$$;


ALTER PROCEDURE financial.sp_transfer(sender_id integer, receiver_id integer, amount bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: accounts; Type: TABLE; Schema: financial; Owner: postgres
--

CREATE TABLE financial.accounts (
    id integer NOT NULL,
    bank_branch character varying(10) NOT NULL,
    account_number character varying(10) NOT NULL,
    balance bigint DEFAULT 0.0 NOT NULL,
    account_type financial.account_types NOT NULL
);


ALTER TABLE financial.accounts OWNER TO postgres;

--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: financial; Owner: postgres
--

ALTER TABLE financial.accounts ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME financial.accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: transactions; Type: TABLE; Schema: financial; Owner: postgres
--

CREATE TABLE financial.transactions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    from_account_id integer NOT NULL,
    to_account_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    value bigint DEFAULT 0.0,
    transaction_type financial.transaction_types NOT NULL
);


ALTER TABLE financial.transactions OWNER TO postgres;

--
-- Name: transactions_queue; Type: TABLE; Schema: financial; Owner: postgres
--

CREATE TABLE financial.transactions_queue (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    from_account_id integer NOT NULL,
    to_account_id integer NOT NULL,
    amount bigint DEFAULT 0.0,
    operation_status financial.operations_status_list DEFAULT 'pending'::financial.operations_status_list NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE financial.transactions_queue OWNER TO postgres;

--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: financial; Owner: postgres
--

ALTER TABLE ONLY financial.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: transactions transactions_pkey; Type: CONSTRAINT; Schema: financial; Owner: postgres
--

ALTER TABLE ONLY financial.transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: transactions_queue transactions_queue_pkey; Type: CONSTRAINT; Schema: financial; Owner: postgres
--

ALTER TABLE ONLY financial.transactions_queue
    ADD CONSTRAINT transactions_queue_pkey PRIMARY KEY (id);


--
-- Name: transactions fk_account; Type: FK CONSTRAINT; Schema: financial; Owner: postgres
--

ALTER TABLE ONLY financial.transactions
    ADD CONSTRAINT fk_account FOREIGN KEY (to_account_id) REFERENCES financial.accounts(id);


--
-- Name: transactions fk_account_from; Type: FK CONSTRAINT; Schema: financial; Owner: postgres
--

ALTER TABLE ONLY financial.transactions
    ADD CONSTRAINT fk_account_from FOREIGN KEY (from_account_id) REFERENCES financial.accounts(id);


--
-- PostgreSQL database dump complete
--

