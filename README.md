# pgbank

#### Setup

`docker-compose up -d`

Acesse o banco com sua IDE favorita. lembrando que a porta é a *55432*

#### Refrências

* https://hub.docker.com/_/postgres
* https://www.cyberciti.biz/faq/how-to-run-command-or-code-in-parallel-in-bash-shell-under-linux-or-unix/
* pgbench
    * https://www.postgresql.org/docs/10/pgbench.html
    * https://severalnines.com/blog/benchmarking-postgresql-performance
* Jobs
    * https://github.com/citusdata/pg_cron
    * https://www.citusdata.com/blog/2016/09/09/pgcron-run-periodic-jobs-in-postgres/
    * https://sqlbackupandftp.com/blog/postgresql-job-scheduler

#### Desafio

Implementar solução para realizar transferências financeiras entre contas com alto volume de concorrência. Transferência é a retirada de saldo disponível de uma conta para outra sem que a conta de origem fique negativa.

Criar um modelo de dados que suporte:
- 70% das contas (tipo 1) fazendo de 1 a 3 transferências concorrentes da mesma para outras contas.
- 30% das contas (tipo 2) fazendo milhares de transferências concorrentes da mesma para outras contas.
- A consulta de saldo precisa estar disponível em tempo real.
- Alta vazão de operações concorrentes por minuto.

Restrições:
- Nenhuma conta pode ficar com saldo negativo.
- As transações precisam ocorrer dentro de uma janela de tempo de 11 horas.
- Os benchmarks precisam iniciar com a base já contendo 2 milhões de operações distribuídas entre as contas de tipo 1 e 2.

Entregáveis:
- Validações de consistência das transações.
- Modelagem do banco, index, procedures, views ou qualquer outra coisa que fizer sentido para a solução do problema.
- Um benchmark mostrando a quantidade de transações concorrentes por segundo que o sistema suporta para os 2 tipos de contas proposto.
- Um benchmark mostrando o volume máximo que o sistema suporta, dentro da janela de operação, para os 2 tipos de conta.
- Documentação explicando a solução, o setup do ambiente para replicação, listagem das ferramentas e fontes de consultas utilizadas no desafio.

Independente do tempo que vai demorar, estamos interessados em observar a qualidade da solução. Peço só que você me mande uma previsão de quando conseguiria finalizá-lo.

Quando tiver concluído, me envie o link para corrigirmos a sua solução e te passar um feedback.
